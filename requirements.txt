jsonschema==3.2.0
#jsonschema-3.2.0-py2.py3
six

# https://github.com/okfn/docker-ckan/issues/84
# apk add geos
# see also ckanext-spatial requirements

ckantoolkit
GeoAlchemy>=0.6
GeoAlchemy2==0.5.0

# Shapely in review is 1.3.1, in prod 1.7.1
shapely==1.3.0
pyproj==1.9.3
OWSLib==0.18.0

# lxml review 3.3.5 prod 4.6.1
lxml>=2.3
argparse
pyparsing>=2.1.10
requests>=1.1.0

# for uploading view configurations to bucket
google-cloud-storage==1.13.1