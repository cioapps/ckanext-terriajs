
from ckanext.jsonschema.indexer_controller import ICustomIndexerController, IndexOperationType
import ckanext.jsonschema.bucket_indexer as _bi
import ckan.plugins as p
import ckan.plugins.toolkit as toolkit
import ckanext.jsonschema.view_tools as _vt
import ckanext.jsonschema.constants as _c
import ckanext.terriajs.constants as _tc
import ckanext.terriajs.utils as _tu
import logging

logger = logging.getLogger(__name__)

#############################################

import json
config = toolkit.config



class TerriajsBucketIndexer(p.SingletonPlugin):
    p.implements(p.IConfigurer)
    # implement the custom indexer interface
    p.implements(ICustomIndexerController, inherit=True)
                                            
    bi = _bi.BucketIndexer(
        _c.BUCKET_NAME, _tc.BUCKET_TERRIAJS_VIEWS_PATH)

    # IConfigurer
    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'ckanext-jsonschema')

    
    # def get_input_types(self):
    #     return input_types.keys()

    # def get_supported_types(self):
    #     return supported_types.keys()

    # def get_supported_resource_types(self):
    #     return supported_resource_types.keys()

    # def get_clonable_resource_types(self):
    #     return clonable_resources_types.keys()

    priority = 3
    def custom_index_view(self, operation, document, package, view):
        if operation == IndexOperationType.CREATE:
            view_jsonschema_body_resolved = json.loads(
                document.get(_c.SCHEMA_BODY_KEY))
            is_package_public = package.get('private') == False
            self.bucket_index_create(
                is_package_public, view, view_jsonschema_body_resolved)
        elif operation == IndexOperationType.DELETE:
            self.bucket_index_delete(view)

    def bucket_index_delete(self, view):
        view_id = view.get('id')
        self.bi.delete_body(view_id)

    def bucket_index_create(self, is_package_public, view, view_jsonschema_body_resolved):
        if is_package_public:
            operation = _c.UPLOAD_OPERATION_KEY
        else:
            operation = _c.DELETE_OPERATION_KEY

        try:
            if _vt.get_view_type(view) in _tc.VIRTUAL_ITEMS:
                if operation == _c.UPLOAD_OPERATION_KEY:
                    _tu.upload_remote_group(self.bi,
                                            view, view_jsonschema_body_resolved)
                else:
                    _tu.delete_remote_group(self.bi,
                                            view, view_jsonschema_body_resolved)
            else:
                if operation == _c.UPLOAD_OPERATION_KEY:
                    self.bi.upload_body(
                        view_jsonschema_body_resolved, view.get('id'))
                else:
                    self.bi.delete_body(view.get('id'))

        except Exception as e:
            logger.error(str(e))
