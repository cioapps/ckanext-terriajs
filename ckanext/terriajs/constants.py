# from sqlalchemy.sql.expression import true
import ckan.plugins.toolkit as toolkit
config = toolkit.config
import json
import os
import ckanext.jsonschema.constants as _c
path = os.path
PATH_ROOT=path.realpath(path.join(path.dirname(__file__),'..'))

# (Internal)
# this indicate the type of the view stored into the DB
TYPE='terriajs'

############################# TODO IN CONFIG, REMOVE?
# (Optional)
DEFAULT_TITLE=config.get('ckanext.terriajs.default.title', 'Map')
ICON=config.get('ckanext.terriajs.icon', 'globe')
# (Optional)
# Can cause problems with missing mapped types.
ALWAYS_AVAILABLE = config.get('ckanext.terriajs.always_available', True)
#######################################################################################

# MANDATORY
# TERRIAJS base url to reach terriajs
TERRIAJS_URL=config.get('ckanext.terriajs.url', 'https://localhost/terriajs')

# MANDATORY
# (ref to type-mapping.json)
SCHEMA_TYPE_MAPPING_FILE=config.get('ckanext.terriajs.schema.type_mapping','{}/../type-mapping.json'.format(PATH_ROOT))

# (Optional)
# fields to do not interpolate with jinja2 (f.e. they are a template of other type)
FIELDS_TO_SKIP = config.get('ckanext.terriajs.skip.fields', ['featureInfoTemplate'])

# (Optional)
# this flag will prevent resource_view_clear action if enabled. (True by Default)
PREVENT_CLEAR_ALL=config.get('ckanext.terriajs.prevent_clear_all', True)


# (Optional)
# List of formats supported for view auto creation (create the view when create the resource)
# NOTE may require extensions to support other formats at the b.e.
# in case you want to override/customize it should be a json array like:
# ckanext.terriajs.default.formats=["csv","wms","mvt","test"]
DEFAULT_FORMATS = config.get('ckanext.terriajs.default.formats', ['csv','wms','mvt','terriajs-group'])
if isinstance(DEFAULT_FORMATS,str):
   # log.debug("DEFAULT_FORMATS is still a string: {}".format(PATH_SCHEMA))
   DEFAULT_FORMATS = json.loads(DEFAULT_FORMATS)
if not isinstance(DEFAULT_FORMATS,list):
   raise Exception('DEFAULT_FORMATS should be an array of valid format strings')


FILENAME_CONFIG = 'config.json'
FILENAME_REGISTRY = 'registry.json'

PATH_SETUP = path.realpath(config.get('ckanext.terriajs.path.setup', path.join(PATH_ROOT, 'terriajs', 'setup')))
PATH_CONFIG = path.realpath(config.get('ckanext.terriajs.path.config', path.join(PATH_ROOT,'terriajs', 'public')))
PATH_REGISTRY = path.realpath(config.get('ckanext.terriajs.path.config', path.join(PATH_ROOT,'terriajs', 'public')))

PATH_TEMPLATE = path.realpath(path.join(PATH_SETUP,'template'))
PATH_SCHEMA = path.realpath(path.join(PATH_SETUP,'schema'))
PATH_MODULE = path.realpath(path.join(PATH_SETUP,'module'))


# (Internal)
# use this type to define a group into terria hierarchy
# type used to define a group of pointers (to a set of views). (resolved internally)
LAZY_GROUP_TYPE = 'terriajs-group'

# (Internal)
# type used internally to define a pointer to a view. (resolved internally)
LAZY_ITEM_TYPE = 'terriajs-view'

# (Internal)
# type to use as ckan resource when you would like to be free to write the 'full view' not only an item
# it may match one of the items into 
CATALOG_TYPE = 'terriajs-catalog'

# REST paths
REST_MAPPING_PATH='/{}/mapping'.format(TYPE)
REST_SCHEMA_PATH='/{}/schema'.format(TYPE)
# TODO REST_*_PATH
# document PAGE_SIZE
PAGE_SIZE = 15

##############################
# (Internal)
# VIEW SCHEMA MODEL
# NOTE the actual values below are also affecting
# queries and js, think twice before you change them 
TERRIAJS_CONFIG_KEY='terriajs_config'
TERRIAJS_TYPE_KEY='terriajs_type'
TERRIAJS_SCHEMA_KEY='terriajs_schema'
TERRIAJS_URL_KEY='terriajs_url'

##############################
# Initialized at config time

# (Internal)
# to hold the type-mapping file content as a dict
TYPE_MAPPING = {}
# (Internal)
#  Will contain the types defined into the file type-mapping
FORMATS = {}
# (Internal)
#  Will contain the schema and template defined with the type-mapping
JSON_CATALOG = {}

CAPABILITIES_ANALYSIS = 'analysis'
CAPABILITIES_CROP = 'crop'
# these special jsonschema types are used to navigate through the terria view catalog

# "fao-maps-review-catalog-mirror"
BUCKET_TERRIAJS_VIEWS_PATH = '{}/terriajs'.format(
    _c.BUCKET_RELATIVE_PATH) if _c.BUCKET_RELATIVE_PATH else 'terriajs'
VIRTUAL_ITEMS = ['terriajs-group', 'group']
API_BASE_URL_BUCKET = config.get(
    'ckanext.terriajs.api_base_url_bucket', 'https://storage.googleapis.com')
REMOTE_CONFIG_TYPES = ['remote-config-group', 'remote-config-item']