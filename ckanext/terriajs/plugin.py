#from ckanext.jsonschema.indexer_controller import ICustomIndexerController, IndexOperationType
import ckanext.terriajs.neo4j_helper as _nh
import copy

import ckan.plugins as p
import ckan.plugins.toolkit as toolkit

_ = toolkit._
g = toolkit.g
config = toolkit.config
import logging

import ckan.lib.navl.dictization_functions as df
import ckan.lib.helpers as helpers

import ckanext.jsonschema.bucket_indexer as _bi
import ckanext.jsonschema.constants as _c
import ckanext.jsonschema.interfaces as _i
import ckanext.jsonschema.tools as _t
import ckanext.jsonschema.utils as _u
import ckanext.jsonschema.validators as _v
import ckanext.jsonschema.view_tools as _vt
import ckanext.terriajs.constants as _tc
import ckanext.terriajs.tools as _tt
import ckanext.terriajs.utils as _tu
import ckanext.terriajs.logic.get as get
from ckan.logic.converters import convert_to_json_if_string
from flask import abort
import requests
import json
#import uuid

get_validator = toolkit.get_validator
not_empty = get_validator('not_empty')
ignore_empty = get_validator('ignore_empty')


log = logging.getLogger(__name__)

class TerriajsPlugin(p.SingletonPlugin):
    p.implements(p.IConfigurer)
    #p.implements(p.IConfigurable)
    p.implements(p.IBlueprint)
    #p.implements(p.ITemplateHelpers)
    #p.implements(p.IResourceUrlChange)
    # p.implements(p.IDomainObjectModification, inherit=True)
    p.implements(p.IActions)
    p.implements(p.IResourceView)
    p.implements(_i.IJsonschemaView, inherit=True)

    #IActions
    def get_actions(self):
        actions = {
            'jsonschema_spatial_search': get.spatial_search,
            'jsonschema_spatial_wms_bbox': get.spatial_wms_bbox
            #'resource_create_default_resource_views': create.resource_create_default_resource_views
            #'resource_view_delete': delete.resource_view_delete, # TODO REMOVE
            #'resource_view_update': delete.resource_view_update
        }
        return actions


    # IBlueprint
    def get_blueprint(self):
        return get.terriajs

    # IConfigurer
    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'ckanext-terriajs')

        self.config = _u._json_load(_tc.PATH_CONFIG, _tc.FILENAME_CONFIG)



    # IJsonschemaView
    def register_jsonschema_resources(self):
        
        _t.add_schemas_to_catalog(_tc.PATH_SCHEMA)
        log.info('Registered schemas into jsonschema\'s registry')

        _t.add_templates_to_catalog(_tc.PATH_TEMPLATE)
        log.info('Registered templates into jsonschema\'s registry')
        
        _t.add_modules_to_catalog(_tc.PATH_MODULE)
        log.info('Registered modules into jsonschema\'s registry')

        _t.add_to_registry(_tc.PATH_REGISTRY, _tc.FILENAME_REGISTRY)
        log.info('Added terriajs registry entries to into jsonschema\'s registry')

    def resolve(self, view_body, view, args={}):

        force = False
        force_to = False

        if args:
            force = args.get('force', False)
            force_to = args.get('force_to', False)

        model = self.get_model(view)

        _terriajs_config = interpolate_fields(model, view_body)     

        _config = get.resolve(_terriajs_config, force, force_to)
        return _config


    def get_model(self, view):
        model = _vt.get_model(view.get('package_id'), view.get('resource_id'))
        view_opt = _vt.get_view_opt(view)
        model.update({
            _tc.TYPE :{'base_url': view_opt.get('base_url') } # TODO replace with opt.base_url?? 
        })

        return model


    def wrap_view(self, view_body, view, args):
               
        view_opts = _vt.get_view_opt(view)

        camera={
            'east': view_opts.get('east',180),
            'west': view_opts.get('west',-180),
            'north': view_opts.get('north',90),
            'south': view_opts.get('south',-90)
        }

        # TODO: This is because we don't have a proper registration of terriajs items into the registry
        # Replace with get_template_of

        _config = copy.deepcopy(_t.get_template_of(_tc.CATALOG_TYPE))
        _config['catalog'].append(view_body)
        _config.update({'homeCamera': camera})

        return _config

    #bi = _bi.BucketIndexer(_c.BUCKET_NAME, _tc.BUCKET_TERRIAJS_VIEWS_PATH)

    # def delete_index(self, view):
    #     self.bucket_index_delete(view)

    # def create_index(self, document, package, view):
    #     # view_jsonschema_body_resolved = json.loads(
    #     #     document.get(_c.SCHEMA_BODY_KEY))
    #     # is_package_public = package.get('private') == False
    #     # self.bucket_index_create(is_package_public, view,
    #     #                   view_jsonschema_body_resolved)

    #     # To prevent possible sideeffect caused by previos call
    #     view_jsonschema_body_resolved = json.loads(
    #         document.get(_c.SCHEMA_BODY_KEY))
    #     self.neo4jIndex(view, view_jsonschema_body_resolved, document)

    # from ckanext.jsonschema.indexer_controller import ICustomIndexerController, IndexOperationType

    # def custom_index_view(self, operation, document, package, view):
    #     if operation == IndexOperationType.CREATE:
    #         self.create_index(document, package, view)

    #     elif operation == IndexOperationType.DELETE:
    #         self.delete_index(view)

        #self.before_index_view(document=document, organization=None, package=package, resource=None, view=obj)

    def before_index_view(self, document, organization, package, resource, view):

        document = _tu.enrich_with_capabilities(document, resource)



        # if _vt.get_view_type(view) in _tc.VIRTUAL_ITEMS:
        #     items = view_jsonschema_body_resolved.get('items')
        #     log.info("before processing items type - {} - items value: {}".format(type(items), items))
        #     updated_items = []
        #     if items:
        #         updated_items = _tu.process_items(items)
        #         log.info("After processing items type - {} - items value: {}".format(type(items), items))
        #     view_jsonschema_body_resolved['items'] = updated_items
        #     #view_jsonschema_body_resolved_copy = json.loads(
        #     #    document.get(_c.SCHEMA_BODY_KEY))
        #     #document[_c.SCHEMA_BODY_KEY] = json.dumps(view_jsonschema_body_resolved)
        #     # In case of a sideeffect could be caused above
        #     #view_jsonschema_body_resolved = view_jsonschema_body_resolved_copy
        # is_package_public = package.get('private') == False
        # #self.bucket_index(is_package_public, view,
        # #                  view_jsonschema_body_resolved)
        # #By doing this, we get rid of the temporary object that has been changed, otherwise we need to deep copy items lists inside bucket indexer

        #view_jsonschema_body_resolved = json.loads(document.get(_c.SCHEMA_BODY_KEY))

        # self.bucket_index_cloudstorage(document, package, view)
        return document
        # deprecated
        # calculate_bbox()

        # return pkg_dict

    # def bucket_index_delete(self, view):
    #     view_id = view.get('id')
    #     self.bi.delete_body(view_id)

    # def bucket_index_create(self, is_package_public, view, view_jsonschema_body_resolved):
    #     if is_package_public:
    #         operation = _c.UPLOAD_OPERATION_KEY
    #     else:
    #         operation = _c.DELETE_OPERATION_KEY

    #     try:
    #         if _vt.get_view_type(view) in _tc.VIRTUAL_ITEMS:
    #             if operation == _c.UPLOAD_OPERATION_KEY:
    #                 _tu.upload_remote_group(self.bi,
    #                                         view, view_jsonschema_body_resolved)
    #             else:
    #                 _tu.delete_remote_group(self.bi,
    #                                         view, view_jsonschema_body_resolved)
    #         else:
    #             if operation == _c.UPLOAD_OPERATION_KEY:
    #                 self.bi.upload_body(
    #                     view_jsonschema_body_resolved, view.get('id'))
    #             else:
    #                 self.bi.delete_body(view.get('id'))

    #     except Exception as e:
    #         log.error(str(e))

    # def bucket_index_cloudstorage(self, document, package, view):
    #     try:
    #         import ckanext.cloudstorage.constants as _cs
    #         view_jsonschema_body_resolved = json.loads(
    #             document.get(_c.SCHEMA_BODY_KEY))
    #         bucket_name = _cs.PREFIX + document.get('org_name')
    #         bh = _bh.BucketHelper(bucket_name, _c.BUCKET_VIEWS_PATH)
    #         bucket_exist = bh.check_bucket_exists()
    #         if bucket_exist:
    #             metadata = {
    #                 "active": True,
    #                 "organization_id": document.get("org_id"),
    #                 "package_id": document.get("pkg_id"),
    #                 "resource_id": document.get("res_id"),
    #                 "owner": toolkit.c.user
    #             }
    #             _tu.upload_remote_group(bh,
    #                         view, view_jsonschema_body_resolved, metadata)
    #             if package.get('private') == False:
    #                 bh.set_blob_public(view.get('id'))
    #             else:
    #                 bh.set_blob_public(view.get('id'), False)
    #     except Exception as e:
    #         log.error("An error occured : {}".format(e))


    # def before_delete_index_view(self, view):
    #     view_id = view.get('id')
    #     self.bi.delete_body(view_id)

    # IDomainObjectModification
    # def notify(self, entity, operation):
    #     #TODO register view delete notification (not watched by this)
    #     u'''
    #     Send a notification on entity modification.
    #     :param entity: instance of module.Package.
    #     :param operation: 'new', 'changed' or 'deleted'.
    #     '''
    #     # if not isinstance(entity, model.Package):
    #     #     return

    #     # log.debug('Notified of package event: %s %s', entity.name, operation)
    #     pass
    
    #IResourceUrlChange
    # def notify(self, resource):
    #     # Receives notification of changed URL on a resource.
    #     # NOTE: this is not needed: Using templates and resolving at runtime the url.
    #     pass

    # IResourceView
    def info(self):

        def default_jsonschema_fields(key, data, errors, context):

            # When creating a view by hand, we don't have the resource in the context
            _data = df.unflatten(data)

            # if 'resource' not in context or 'view_type' in _data:
            #     return

            # If we have the id, the view exists so we don't need to set defaults
            if 'id' in _data or 'resource' not in context:
                return

            #package_id = context['package'].id
            #resource_id = _data['resource_id']
            #view_id =  uuid.uuid4()
            #result = "/".join(package_id,resource_id, view_id)
            #view_body = _vt.get_view_body(_data)
            #view_body['id'] = result
            #_data['id'] = result
            resource = context['resource'].as_dict()

            plugin = _vt.get_jsonschema_view_plugin(_data.get('view_type'))
            data_dict = plugin.setup_template_variables(context, {'resource_view': _data, 'resource': resource})
            
            # resource_view = data_dict.get('resource_view') 
            
            # if not _c.SCHEMA_TYPE_KEY in _data:
            #     _data[_c.SCHEMA_TYPE_KEY] = _vt.get_view_type(resource_view) 

            # if not _c.SCHEMA_BODY_KEY in _data:
            #     _data[_c.SCHEMA_BODY_KEY] = _vt.get_view_body(resource_view) 
                
            # if not _c.SCHEMA_OPT_KEY in _data:    
            #     _data[_c.SCHEMA_OPT_KEY] = _vt.get_view_opt(resource_view) 
            
            data.update(df.flatten_dict(_data))


        info = {
            u'iframed': False,
            u'name': _tc.TYPE,
            u'schema': {
                _c.SCHEMA_TYPE_KEY: [ignore_empty], # import
                _c.SCHEMA_BODY_KEY: [default_jsonschema_fields, not_empty, _tt.id_validator, _v.view_schema_check, convert_to_json_if_string],
                _c.SCHEMA_OPT_KEY: [convert_to_json_if_string],
                "selected_jsonschema_type": [ignore_empty]
            },
            u'requires_datastore': False
        }
        
        plugin_info = _vt.get_info(self.config)

        info.update(plugin_info)

        return info
    

    def can_view(self, data_dict):
        return _vt.can_view(self.config, data_dict)

    def setup_template_variables(self, context, data_dict):
    #TODO Do we need this? Could conflict with package's setup_template_variables
            # TODO MOVE IN JSONSCHEMA PLUGIN

        resource_view = data_dict.get('resource_view', {})

        view_jsonschema_type = _vt.get_view_type(resource_view)
        view_body = _t.as_dict(_vt.get_view_body(resource_view))
        view_opt = _t.as_dict(_vt.get_view_opt(resource_view))

        if not view_jsonschema_type: # the view doesn't exist

            resource = data_dict.get('resource')
            resource_format = resource.get('format')
            resource_jsonschema_type = _t.get_resource_type(resource)

            view_configuration =_vt.get_view_configuration(self.config, resource_format, resource_jsonschema_type)
            
            # we force the first jsonschema type, even if there are more matches
            view_jsonschema_type = view_configuration.get(_c.VIEW_JSONSCHEMA_TYPE)[0]
            view_body = _t.as_dict(_t.get_template_of(view_jsonschema_type))

            view_opt = self.config_options(view_jsonschema_type)
        try:
            view_opt = check_terriajs_url(view_opt)
            resource_view = data_dict.get('resource_view')
            resource_view.update({
                    _c.SCHEMA_TYPE_KEY: view_jsonschema_type,
                    _c.SCHEMA_BODY_KEY: view_body,
                    _c.SCHEMA_OPT_KEY: view_opt,
                    _c.JSON_SCHEMA_KEY: _t.get_schema_of(view_jsonschema_type)
                })

            return data_dict
        except Exception as e:
            abort(404, e.message)
    
    def view_template(self, context, data_dict):
        return 'terriajs.html'

    def form_template(self, context, data_dict):
        return 'terriajs_form.html'

    def config_options(self, view_jsonschema_type):
        # fetch options from template
        template_opts =_t.get_opt_template_of(view_jsonschema_type) 

        if template_opts:
            view_opt = _t.as_dict(template_opts)
        else:
            # fetch options from configuration
            config_opts =_vt.get_opt(self.config)
            view_opt = _t.as_dict(config_opts)

        view_opt = check_terriajs_url(view_opt)

        return view_opt

def check_terriajs_url(view_opt):
    # ensure existence of terriajs url
    if not view_opt.get(_tc.TERRIAJS_URL_KEY):
        # TODO maybe we want to try to give priority to config.json
        # and fallback to production.ini when missing
        view_opt[_tc.TERRIAJS_URL_KEY] = _tc.TERRIAJS_URL
    return view_opt

def interpolate_fields(model, template):
        
    import sys
    _py3=False
    if (sys.version_info > (3, 0)):
        _py3=True
    import jinja2
    Environment = jinja2.environment.Environment
    FunctionLoader = jinja2.loaders.FunctionLoader 
    TemplateSyntaxError = jinja2.TemplateSyntaxError
    ###########################################################################
    # Jinja2 template
    ###########################################################################
    

    def functionLoader(name):
        if _py3:
            # TODO check python3 compatibility 'unicode' may disappear?
            if isinstance(item[f],(str)):
                return item[name]
        elif isinstance(item[f],(str,unicode)):
            return item[name].decode('UTF-8')
        return item[name]

    env = Environment(
                loader=FunctionLoader(functionLoader),
                autoescape=True,
                trim_blocks=False,
                keep_trailing_newline=True)

    FIELDS_TO_SKIP = config.get('ckanext.terriajs.skip.fields', ['featureInfoTemplate'])

    
    if 'catalog' in template:
        wrapped = True
        items = template['catalog']
    else:
        wrapped = False
        items = [template]

    for item in items:
        for f in item.keys():
            if f in FIELDS_TO_SKIP:
                continue
            

            interpolate = False

            if _py3:
                # TODO check python3 compatibility 'unicode' may disappear?
                if isinstance(item[f],(str)):
                    interpolate = True
            elif isinstance(item[f],(str,unicode)):
                    interpolate = True
            
            
            if interpolate:
                try:
                    _template = env.get_template(f)
                    item[f] = _template.render(model)
                except TemplateSyntaxError as e:
                    raise Exception(_('Unable to interpolate field \'{}\' line \'{}\'\nError:{}'.format(f,str(e.lineno),str(e))))
                except Exception as e:
                    raise Exception(_('Unable to interpolate field \'{}\': {}'.format(f,str(e))))

    if wrapped:
        template['catalog'] = items
    else:
        template = items[0]

    return template
    ###########################################################################

def calculate_bbox(pkg_dict, resource):
    resource_format = resource.get('format')
    process_bbox = resource_format.lower() == "wms"
    if not process_bbox:
        return pkg_dict

    from shapely import geometry
    from shapely.geometry import Polygon

    # Start from already existing bbox
    try:
        bounds = (pkg_dict['maxy'], pkg_dict['miny'],
                    pkg_dict['maxx'], pkg_dict['minx'])
        bbox = geometry.box(*bounds, ccw=True)
    except:
        bbox = Polygon()

    wms_base_url = resource.get('url')
    resource_name = resource.get('name')
    bounds = _tt.calculate_bbox(wms_base_url, resource_name)
    bbox = bbox.union(geometry.box(*bounds, ccw=True))

    minx, miny, maxx, maxy = bbox.bounds
    pkg_dict['maxy'] = maxy
    pkg_dict['miny'] = miny
    pkg_dict['maxx'] = maxx
    pkg_dict['minx'] = minx
    # bbox has .area property
    pkg_dict['bbox_area'] = (pkg_dict['maxx'] - pkg_dict['minx']) * \
        (pkg_dict['maxy'] - pkg_dict['miny'])
