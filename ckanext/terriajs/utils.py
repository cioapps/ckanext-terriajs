
import os
import json
import logging
import ckan.plugins.toolkit as toolkit
import ckanext.jsonschema.bucket_helper as _bh
import ckanext.jsonschema.constants as _c
import ckanext.jsonschema.view_tools as _vt
import ckanext.jsonschema.utils as _u
import ckanext.terriajs.constants as _tc


logger = logging.getLogger(__name__)

def _json_load(folder, name):
    '''
    Maight be used with caution, the 'folder' param is considered trusted (may never be exposed as parameter)
    '''
    try:
        file=os.path.realpath(os.path.join(folder,name))
        # ensure it's a file and is readable
        isfile=os.path.isfile(file)
        # ensure it's a subfolder of the project
        issafe = os.path.commonprefix([file, folder]) == folder
        if isfile and issafe:
            with open(file) as s:
                return json.load(s)
        else:
            return None
    except Exception as ex:
        raise Exception(_("Schema named: {} not found, please check your schema path folder: {}".format(name,str(ex))))


def _read_all_json(root):
    import os
    _dict={}
    for subdir, dirs, files in os.walk(root):
        for filename in files:
            if filename.endswith('.json'):
                # filename=os.path.realpath(os.path.join(subdir,filename))
                _dict[filename]=_json_load(root,filename)
    return _dict

def enrich_with_capabilities(document, resource):
    capabilities = []
    resource_format = resource.get('format')
    if resource_format.lower() == "zip":
        capabilities.append(_tc.CAPABILITIES_ANALYSIS)
        capabilities.append(_tc.CAPABILITIES_CROP)
    elif resource_format.lower() == "wms":
        capabilities.append(_tc.CAPABILITIES_ANALYSIS)
    elif resource_format.lower() == "wfs":
        capabilities.append(_tc.CAPABILITIES_ANALYSIS)
        capabilities.append(_tc.CAPABILITIES_CROP)
    # To be safe
    if document.get('capabilities') is None:
        document['capabilities'] = []
    document.get('capabilities').extend(capabilities)
    return document


def update_items(bi, items, operation):
    updated_items = []
    for item in items:
        item_type = item.get('type', '')
        # refactor this
        if item_type in _tc.VIRTUAL_ITEMS:
            group_items = item.get('items')
            if not group_items:
                updated_items.append(item)  # No items to update
                continue
            updated_items.append({
                "name": item.get('name'),
                "type": item_type,
                "items": update_items(bi, group_items, operation)
            })
            continue
        elif item_type not in _tc.REMOTE_CONFIG_TYPES :
            updated_items.append(item)  # Not changed
            continue
        url = item.get('url')
        try:
            #fetched_item = _bh.fetch_view(url)
            fetched_item = _u.fetch_view(url)
        except Exception as e:  # For transition: The index for view might not be there in the first reindexing, could also be deleted
            logger.warn(str(e))
            continue
        # fetched_item[_c.SCHEMA_BODY_KEY]
        configuration = _vt.get_view_body(fetched_item)
        is_metadata_public = True if fetched_item.get(
            'package_capacity') == 'public' else False
        if not is_metadata_public:  # if not public, do not point to bucket instead point to ckan, just update the name in case it has changed
            copied_item = dict(item)
            copied_item.update({
                "name": configuration.get('name'),
                "capacity": "private"
            })
            updated_items.append(copied_item)
            continue

        # configuration_items = configuration.get('items')

        # if configuration_items:  # if there are nested items -> recurse
        #    configuration['items'] = self.update_items(bucket_indexer,
        #        configuration.get('items'), operation)

        id = fetched_item.get('view_id')
        filename = "{}.{}".format(id, "json")
        # if operation == _c.DELETE_OPERATION_KEY:
        #    self.bucket_helper.delete_bucket_file(filename)
        # elif operation == _c.UPLOAD_OPERATION_KEY:
        #    serialized_configuration = json.dumps(configuration)
        #    self.bucket_helper.upload_bucket_file(
        #        serialized_configuration, filename)

        if operation == _c.UPLOAD_OPERATION_KEY:
            if configuration.get('items') is None:  # if it is an item
                updated_items.append(configuration)
                continue
            # if it is another group
            bucket_file_url = create_bucket_api_request_url(bi,
                filename)
            updated_items.append({
                "url":  bucket_file_url,
                "type": item.get('type'),
                "name": configuration.get('name'),
                "capacity": "public"
            })
    return updated_items

# Deprecatd, graph logic
# def process_items(items):
#     """
#     Processes a list of items by examining each item's type and performing actions based on the type.

#     - If an item is a 'virtual item' (contains a list of sub-items), it recursively processes 
#       the sub-items.
#     - If an item is a 'remote config type' (linked to a URL), it fetches additional details and 
#       updates the item based on its privacy setting.
#     - For all other item types, the item is left unchanged.

#     Args:
#         items (list): A list of item dictionaries, where each item has properties like 'name', 
#                       'type', and 'url'.

#     Returns:
#         list: A list of updated items with modifications based on the processing rules.
#     """
#     updated_items = []  # List to store processed items

#     for item in items:
#         item_type = item.get('type', '')  # Get item type, or an empty string if 'type' isn't found

#         # If the item is a virtual item type, it may contain a list of sub-items
#         if item_type in _tc.VIRTUAL_ITEMS:
#             group_items = item.get('items')  # Retrieve sub-items if present

#             if not group_items:  # If no sub-items exist, add item as-is
#                 updated_items.append(item)
#                 continue  # Skip further processing for this item

#             # Process sub-items recursively and add the processed result
#             updated_items.append({
#                 "name": item.get('name'),
#                 "type": item_type,
#                 "items": process_items(group_items)  # Recursive call
#             })
#             continue

#         # If the item type is not a remote config type, add it without changes
#         elif item_type not in _tc.REMOTE_CONFIG_TYPES:
#             updated_items.append(item)
#             continue

#         # If the item is a remote config type, additional processing is required
#         elif item_type in _tc.REMOTE_CONFIG_TYPES:
#             url = item.get('url')  # Retrieve URL to fetch more data

#             try:
#                 # Fetch the item details from a remote source
#                 #fetched_item = _bh.fetch_view(url)
#                 fetched_item = _u.fetch_view(url)
#             except Exception as e:
#                 # Log any errors (such as missing index) and skip this item
#                 logger.warning("Failed to fetch item from {}: {}".format(url, str(e)))
#                 continue

#             # Extract the main content or "body" of the fetched item
#             view_body = _vt.get_view_body(fetched_item)

#             # Check if the item's metadata is public or private
#             is_metadata_public = fetched_item.get('package_capacity') == 'public'

#             # If metadata is not public, update the URL to point to a private view on CKAN
#             if not is_metadata_public:
#                 item_id = _u.extract_id_from_url(item.get('url'))  # Get item ID from URL
#                 item.update({
#                     "name": view_body.get('name'),
#                     "capacity": "private",
#                     "url": "{}/get_graph?base_node_id={}".format(base_url, item_id)
#                 })
#             # Add the updated private item to the list
#             updated_items.append(item)
#             continue

#         # Otherwise, handle public items with specific processing
#         else:
#             # Get the unique ID of the fetched item, which will be part of the filename
#             view_id = fetched_item.get('view_id')
#             filename = "{}.json".format(view_id)

#             # Check if the item itself has no sub-items (i.e., it is a single item)
#             if view_body.get('items') is None:
#                 updated_items.append(view_body)  # Add as-is
#                 continue

#             # If the item has sub-items, update it as a group with public access
#             updated_items.append({
#                 "type": item.get('type'),
#                 "name": view_body.get('name'),
#                 "capacity": "public"
#             })

#     return updated_items  # Return the fully processed list of items


def delete_remote_group(bi, view, view_jsonschema_body_resolved):
    if bi.bucket_indexer_disable:
        return
    # with current logic no need to resolve items in case of deletion
    # if it would be necessery to do something like that later please also adjust update_items method for operation = delete
    bi.delete_body(view.get('id'))

    # if self.bucker_indexer_disable:
    #    return
    # operation = _c.DELETE_OPERATION_KEY
    # items = view_jsonschema_body_resolved.get('items')
    # if items:
    #    self.update_items(bucket_indexer,  items, operation)
    # file_name = "{}.json".format(view['id'])
    # self.bucket_helper.delete_bucket_file(file_name)


def upload_remote_group(bi, view, view_jsonschema_body_resolved, metadata=None):
    if bi.bucket_indexer_disable:
        return
    operation = _c.UPLOAD_OPERATION_KEY
    items = view_jsonschema_body_resolved.get('items')
    updated_items = []
    if items:
        updated_items = update_items(bi, items, operation)
        #updated_items = process_items(items) -> works on graph logic, should not be used for static bucket indexing
    file_name = "{}.json".format(view['id'])
    config = view_jsonschema_body_resolved
    config['items'] = updated_items
    bi.bucket_helper.upload_bucket_file(
        json.dumps(config), file_name, metadata)
    

def create_bucket_api_request_url(bi, file_name):
    return "{}/{}/{}/{}".format(_tc.API_BASE_URL_BUCKET, bi.bucket_name, bi.bucket_relative_path, file_name)


